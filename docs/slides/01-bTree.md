## Індекс btree - дерево B tree

<small>Призначення - дані, для яких застосовуються загальні оператори порівняння</small>

CREATE INDEX index_name ON table_name USING btree(column);



<img src="images/btree_with_linked_list.svg" class="floatLeft" width=50%/>