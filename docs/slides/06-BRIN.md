## BRIN - Block Range Index

<small>Призначення - великі об'єми даних</small>

CREATE INDEX ON table_name USING brin(column) WITH (pages_per_range=4);
