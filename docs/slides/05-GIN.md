## GIN - Generalized Inverted Index

<small>Призначення - складові типи данних</small>

CREATE INDEX index_name ON table_name USING GIN (column)



<img src="images/gin.svg" class="floatLeft" width=100% />