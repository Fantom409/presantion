<h2>Хеш індекс</h2>

<small>Призначення - дані, що знаходяться за оператором =</small>

CREATE INDEX index_name ON table_name USING hash(column);